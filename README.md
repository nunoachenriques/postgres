Example of customizing PostgreSQL for GitLab CI
===================

The purpose of this repository is to show how to use GitLab to do
Continuous Integration with a PostgreSQL database.

In order to run this project just fork it on GitLab.com.
Every push will then trigger a new build on GitLab.

---

This is a companion project of <http://doc.gitlab.com/ce/ci/services/postgres.html>.
